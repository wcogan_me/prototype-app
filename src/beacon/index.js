// src/beacon/index.js

import Vue from 'vue'

import API from 'api'

export default {

    delegate: null, 
    beaconRegion: null,
    key: "beacon",

    backgroundMonitoring() {
        var storage = window.localStorage
        var value = storage.getItem(this.key) // Pass a key name to get its value.
        storage.setItem(this.key, int.Parse(value) + 1) // Pass a key name and its value to add or update that key.
        //storage.removeItem(this.key) // Pass a key name to remove that key from storage.
    },

    startMonitoring() {
        var uuid = 'B9407F30-F5F8-466E-AFF9-25556B57FE6D'
        var identifier = 'beaconOnTheMacBooksShelf'
        var major = ''//5;
        var minor = ''//1000;

        var api_participant_token = ''
        var api_participant_email = ''
        var device_id = ''
        var api_url = ''
        var api_version = ''
        var sendMovementData = false

        alert('startMonitoring')
        /*BackgroundBeaconMonitoring.requestPermissions(
            () => { 
                alert('requestPermissions success')

                BackgroundBeaconMonitoring.startService(api_participant_token, api_participant_email, device_id, api_url, api_version, sendMovementData, 
                    () => { 
                        alert('startService success') 
                        BackgroundBeaconMonitoring.startMonitoringRegion(identifier, uuid, major, minor, 
                            () => { alert('startMonitoringRegion success') }, 
                            (e) => { alert(`startMonitoringRegion fail: ${e}`) }
                        )
                    }, 
                    (e) => { alert(`startService fail: ${e}`) }
                )
            }, 
            () => { alert('requestPermissions fail') }
        )

        // var storage = window.localStorage
        // var value = storage.getItem(this.key) // Pass a key name to get its value.
        // alert(value)
        alert('startedMonitoring')
        return;*/
        
        //alert(this.Vue);
        //alert(Vue.cordova.locationManager)
        //alert(cordova.plugins.locationManager)
        //this.logToDom('startMonitoring')
        if (!this.delegate) {
            //this.logToDom('delegate is null')
            this.delegate = new cordova.plugins.locationManager.Delegate()
            //this.logToDom('new delegate')
            
            this.delegate.didEnterRegion = (pluginResult) => {
                this.logToDom('didEnterRegion: ');// + JSON.stringify(pluginResult));
                API.submitRegionEntered()
            }
            this.delegate.didExitRegion = (pluginResult) => {
                this.logToDom('didExitRegion: ');// + JSON.stringify(pluginResult));
            }
            //this.delegate.didDetermineStateForRegion = function (pluginResult) {

            //    logToDom('[DOM] didDetermineStateForRegion: ' + JSON.stringify(pluginResult));

            //    cordova.plugins.locationManager.appendToDeviceLog('[DOM] didDetermineStateForRegion: '
            //        + JSON.stringify(pluginResult));
            //};

            //this.delegate.didStartMonitoringForRegion = function (pluginResult) {
            //    console.log('didStartMonitoringForRegion:', pluginResult);

            //    logToDom('didStartMonitoringForRegion:' + JSON.stringify(pluginResult));
            //};

            this.delegate.didRangeBeaconsInRegion = (pluginResult) => {
                //logToDom('[DOM] didRangeBeaconsInRegion: ' + JSON.stringify(pluginResult));
            }
        }

        if (!this.beaconRegion) {
            //this.logToDom('beaconRegion is null')
            this.beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid)//, major, minor);
            //this.logToDom('new beaconRegion')
        }

        alert(this.delegate)
        cordova.plugins.locationManager.setDelegate(this.delegate)
        //this.logToDom('delegate has been set')

        // required in iOS 8+
        cordova.plugins.locationManager.requestWhenInUseAuthorization()
        // or cordova.plugins.locationManager.requestAlwaysAuthorization()

        //this.logToDom('Pre start')

        cordova.plugins.locationManager.startMonitoringForRegion(this.beaconRegion)
            .fail(function (e) {
                console.error(e)
            })
            .done();

        //this.logToDom('Started monitoring')

        cordova.plugins.locationManager.startRangingBeaconsInRegion(this.beaconRegion)
            .fail(function (e) {
                console.error(e)
            })
            .done();

        //this.logToDom('Started ranging')
    },

    stopMonitoring() {
        logToDom('Shutting down monitoring...');
        /*cordova.plugins.locationManager.stopMonitoringForRegion(beaconRegion)
            .fail(function (e) { console.error(e); })
            .done();
        logToDom('Stopped monitoring');

        cordova.plugins.locationManager.stopRangingBeaconsInRegion(beaconRegion)
            .fail(function (e) { console.error(e); })
            .done();
        logToDom('Stopped ranging');*/
    },

    logToDom(message) {
        //alert(message)
        window.plugins.toast.showLongBottom(message, (a) => {}, (a) => {})
        /*var eventsElement = document.body.querySelector('.events');

        var e = document.createElement('label');
        e.innerText = message;

        var br = document.createElement('br');
        var br2 = document.createElement('br');
        eventsElement.appendChild(e);
        eventsElement.appendChild(br);
        eventsElement.appendChild(br2);*/

        //window.scrollTo(0, window.document.height);
    }
}